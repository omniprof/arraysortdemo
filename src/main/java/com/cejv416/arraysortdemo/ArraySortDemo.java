package com.cejv416.arraysortdemo;

import java.text.NumberFormat;
import java.util.Arrays;

/**
 * Demo of sorting
 *
 * @author Ken Fogel
 */
public class ArraySortDemo {

    public void perform() {

        // Create a large array and fill it with random integers
        int[] stuff = new int[10000000];

        for (int x = 0; x < stuff.length; ++x) {
            stuff[x] = (int) (Math.random() * 100000 + 1);
        }
        
        stuff[stuff.length-1] = 10000000;

        // Search for 100000 in the unsorted array
        System.out.println("Start search of unsorted array:");
        long start = System.nanoTime();
        int foundAt;
        for (foundAt = 0; foundAt < stuff.length; ++foundAt) {
            if (stuff[foundAt] == 10000000) {
                break;
            }
        }
        long end = System.nanoTime();
        System.out.println("Linear search found 100000000 at " + foundAt);
        System.out.println("Time to search = " + NumberFormat.getNumberInstance().format((end - start)/1000000000.0) + " seconds.");

        System.out.println("");

        // Sort the array
        System.out.println("Time to sort an array:");
        start = System.nanoTime();
        Arrays.sort(stuff);
        end = System.nanoTime();
        System.out.println("End Sorting Time = " + NumberFormat.getNumberInstance().format((end - start)/1000000000.0) + " seconds.");

        System.out.println("");

        // Binary search
        System.out.println("Time to search a sorted array");
        start = System.nanoTime();
        foundAt = Arrays.binarySearch(stuff, 10000000);
        end = System.nanoTime();
        System.out.println("end " + end);
        System.out.println("start " + start);
        System.out.println("Binary search found 1000000000 at " + foundAt);
        NumberFormat nf = NumberFormat.getIntegerInstance();
        nf.setMaximumFractionDigits(20);
        System.out.println("Time to search = " + nf.format((end - start)/1000000000.0) + " seconds.");
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ArraySortDemo asd = new ArraySortDemo();
        asd.perform();
        System.exit(0);
    }
}
